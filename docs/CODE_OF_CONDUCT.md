# Code of Conduct

## Table of Contents

1. [Our Pledge](#our-pledge)
2. [Our Standards](#our-standards)
3. [Enforcement Responsibilities](#enforcement-responsibilities)
4. [Scope of Responsibilities](#scope-of-responsibilities)
5. [Conflict Resolution](#conflict-resolution)
6. [Enforcement Guidelines](#enforcement-guidelines)

   1. [Correction](#1-correction)
   2. [Warning](#2-warning)
   3. [Temporary Ban](#3-temporary-ban)
   4. [Permanent Ban](#4-permanent-ban)

7. [Licencing](#licencing)
8. [Attribution](#attribution)

## Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making
participation in our project and our community a harassment-free experience for everyone, regardless of age, body size,
disability, ethnicity, gender identity and expression, level of experience, education, socio-economic status,
nationality, personal appearance, race, religion, or sexual identity and orientation.

## Our Standards

Examples of behaviour that contributes to creating a positive environment include:

- Using welcoming and inclusive language
- Demonstrating empathy and kindness toward other people
- Being respectful of differing opinions, viewpoints, and experiences
- Giving and gracefully accepting constructive feedback
- Accepting responsibility and apologising to those affected by our mistakes, and learning from the experience
- Focusing on what is best not just for us as individuals, but for the overall community

Examples of unacceptable behaviour include:

- The use of sexualised language or imagery, and sexual attention or advances of any kind
- Trolling, insulting or derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others’ private information, such as a physical or email address, without their explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting

## Enforcement Responsibilities

Community leaders are responsible for clarifying and enforcing our standards of acceptable behaviour and will take
appropriate and fair corrective action in response to any behaviour that they deem inappropriate, threatening,
offensive, or harmful.

Community leaders have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits,
issues, and other contributions that are not aligned to this Code of Conduct, and will communicate reasons for
moderation decisions when appropriate.

## Scope of Responsibilities

This Code of Conduct applies within all community spaces, and also applies when an individual is officially
representing the community in public spaces. Examples of representing our community include using an official email
address, posting via an official social media account, or acting as an appointed representative at an online or offline
event.

## Conflict Resolution

We do not believe that all conflict is bad; healthy debate and disagreement often yield positive results. However, it
is never okay to be disrespectful or to engage in behaviour that violates the project’s code of conduct.

If you see someone violating the code of conduct, you are encouraged to address the behaviour directly with those
involved. Many issues can be resolved quickly and easily, and this gives people more control over the outcome of their
dispute. If you are unable to resolve the matter for any reason, or if the behaviour is threatening or harassing,
report it. We are dedicated to providing an environment where participants feel welcome and safe.

Reports should be directed to Alex C. Garth-Dòmhnallach <contact@alexgd.me>, the Community Leader(s) for The Digital
Garden. It is the Community Leader’s duty to receive and address reported violations of the code of conduct. All
complaints will be reviewed and investigated promptly and fairly.

All community leaders are obligated to respect the privacy and security of the reporter of any incident.

## Enforcement Guidelines

Community leaders will follow these Community Impact Guidelines in determining the consequences for any action they
deem in violation of this Code of Conduct:

### 1. Correction

**Community Impact:** Use of inappropriate language or other behaviour deemed unprofessional or unwelcome in the
community.

**Consequence:** A private, written warning from community leaders, providing clarity around the nature of the
violation and an explanation of why the behaviour was inappropriate. A public apology may be requested.

### 2. Warning

**Community Impact:** A violation through a single incident or series of actions.

**Consequence:** A warning with consequences for continued behaviour. No interaction with the people involved,
including unsolicited interaction with those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels like social media. Violating these
terms may lead to a temporary or permanent ban.

### 3. Temporary Ban

**Community Impact:** A serious violation of community standards, including sustained inappropriate behaviour.

**Consequence:** A temporary ban from any sort of interaction or public communication with the community for a
specified period of time. No public or private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period. Violating these terms may lead to a permanent
ban.

### 4. Permanent Ban

**Community Impact:** Demonstrating a pattern of violation of community standards, including sustained inappropriate
behaviour, harassment of an individual, or aggression toward or disparagement of classes of individuals.

**Consequence:** A permanent ban from any sort of public interaction within the community.

## Licencing

This file is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License (`CC BY-SA 4.0`).

As per the terms of the CC BY-SA 4.0 license, if you adapt, remix, transform, or build upon this material, you must
appropriately attribute the original work, including indicating any changes made. Additionally, any derivative work
must be distributed under the same license as the original.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant][cc], version 2.1, available at
<https://www.contributor-covenant.org/version/2/1/code_of_conduct.html>.

The contents of this file draw inspiration from [Google's code of conduct][google-coc].

Community Impact Guidelines were inspired by [Mozilla’s code of conduct enforcement ladder][moz-enforcement].

[cc]: https://www.contributor-covenant.org
[google-coc]: https://opensource.google/documentation/reference/releasing/template/CODE_OF_CONDUCT
[moz-enforcement]: https://github.com/mozilla/diversity
