# Alex's Digital Garden

[![Using Hugo](https://img.shields.io/badge/Using-Hugo-ff4088.svg?style=flat-square&logo=hugo)](https://gohugo.io)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg?style=flat-square&logo=contributorcovenant)](/docs/CODE_OF_CONDUCT.md)
[![Project Licence](https://img.shields.io/badge/Licence-EUPL--1.2-001489.svg?style=flat-square&logo=opensourceinitiative)](/LICENCE)
[![Build Status](https://img.shields.io/gitlab/pipeline-status/agdell%2Fgarden.alexgd.me?style=flat-square&logo=cloudflare&label=Deployment)][project-site]

This repository contains the source code for my [digital garden][project-site], where I document my learnings,
reflections, and creative explorations.

## Table of Contents

1. [Installation](#installation "Instructions for Getting Started")
2. [Usage](#usage "How to use the project")
3. [Configuration](#configuration "How to configure the project so you can use it")
4. [Contributing](#contributing "How you can contribute to the project")
5. [Licence](#licence "Licencing information for the project")

## Installation

Before you can build the garden, you need these dependencies:

1. Git, you can find binaries at <https://git-scm.com/downloads>
2. Hugo, following the [Official Installation Guide][hugo-install]
3. NodeJS, following their [Installation Guide][node-install]

To set up the garden, follow these steps:

1. Clone the repository to your local machine

   ```sh
   git clone --recurse-submodules git@gitlab.com:agdell/garden.alexgd.me.git
   ```

2. Navigate to the project directory

   ```sh
   cd garden.alexgd.me
   ```

## Usage

To view the digital garden locally, follow these steps:

1. Install the node dependencies

   ```sh
   npm clean-install
   ```

2. Run the development script

   ```sh
   npm test
   ```

To generate a production-ready version of the garden, run:

```sh
npm run build
```

The compiled static files will be located in the `public/` directory, ready to be deployed to your hosting platform of
choice.

## Configuration

We use the [Digital Garden Theme][digital-garden], and the look of the site is controlled by that.

You can control what shows up on `domain.com/stack/` by editing [`data/stack.yml`](/data/stack.yml).

## Contributing

We have a [Code of Conduct][coc] for all contributors.

## Licence

The contents of this project are available under the [European Union Public Licence][eupl] (`EUPL-1.2`).

The project's [Code of Conduct][coc] is licenced under the
[Creative Commons Attribution-ShareAlike 4.0 International Licence][cc] (`CC BY-SA 4.0`).

[cc]: /docs/LICENCE.CC-BY-SA-4.0.md "The Creative Commons Attribution-ShareAlike 4.0 Licence"
[coc]: /docs/CODE_OF_CONDUCT.md "Our Code of Conduct, derived from the Contributor Covenant"
[digital-garden]: https://github.com/apvarun/digital-garden-hugo-theme
[eupl]: /docs/LICENCE.EUPL-1.2.md "The European Union Public Licence, version 1.2"
[hugo-install]: https://gohugo.io/installation/ "Installation Instructions for macOS, Linux, Windows, and BSD"
[node-install]: https://nodejs.org/en/learn/getting-started/how-to-install-nodejs "An overview on how to install node"
[project-site]: https://garden.alexgd.me "Alex's Digital Garden"
