---
title: Homepage
---

# Hello 👋

**Welcome to my digital garden!**

A digital garden is like a personal wiki and a knowledge database of thoughts and ideas. Similar to a traditional
garden, a digital one will also container various kinds of content (plants), of which may even be unrelated to each
other. Ideas are not refined, thoughts are not tailored. Here is an excellent write-up about the
[history of digital gardens][1].

Twitter _(Now X)_, for some, is also equivalent to a digital garden. It lets you share thoughts and ideas with
everyone. But how often do you go back to those tweets? Not often. That's why you need a space for your ideas on the
internet **that you own**. Check out [Digital gardens let you cultivate your own little bit of the internet][2] by MIT
technology review.

→ [Read my articles](/articles)\
→ [Learn with me](/learning)

[1]: https://maggieappleton.com/garden-history
[2]: https://www.technologyreview.com/2020/09/03/1007716/digital-gardens-let-you-cultivate-your-own-little-bit-of-the-internet/
